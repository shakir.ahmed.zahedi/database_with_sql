FROM openjdk:11
COPY target/user-admin-server.jar server.jar
ENTRYPOINT ["java","-jar","server.jar"]