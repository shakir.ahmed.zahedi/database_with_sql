package com.example.iam.controller;

import com.example.iam.domain.Domain;
import com.example.iam.domain.Rights;
import com.example.iam.dto.CreateDomainDTO;
import com.example.iam.dto.DomainDTO;
import com.example.iam.dto.UpdateDomainDTO;
import com.example.iam.exceptions.NotFoundException;
import com.example.iam.repo.DomainRepository;
import com.example.iam.repo.RightsRepository;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/domains")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DomainController {

    DomainRepository domainRepository;
    RightsRepository rightsRepository;

    public DomainController(DomainRepository domainRepository, RightsRepository rightsRepository) {
        this.domainRepository = domainRepository;
        this.rightsRepository = rightsRepository;
    }

    @GetMapping
    public List<DomainDTO> all() {
        return StreamSupport.stream(domainRepository.findAll().spliterator(), false)
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    @Transactional
    public DomainDTO getById(@PathVariable("id") String id) throws NotFoundException {
        if (id == null)
            throw new NotFoundException(id);
        return domainRepository.findById(id)
                .map(this::toDTO)
                .orElseThrow(()-> new NotFoundException(id));
    }

    @DeleteMapping("/{id}/{rightName}")
    @Transactional
    public void deleteRightByName(@PathVariable("id") String id, @PathVariable("rightName") String rightName) throws NotFoundException {
        if (id == null)
            throw new NotFoundException(id);
        Domain domain = domainRepository.findById(id).orElseThrow(() -> new NotFoundException(id));
        Rights rights = domain.getRights().stream()
                .filter(r -> r.getKey().equalsIgnoreCase(rightName))
                .findFirst()
                .orElseThrow(()-> new NotFoundException(id));
        domain.getRights().remove(rights);
        rightsRepository.delete(rights);
        domainRepository.save(domain);
    }

    @DeleteMapping("/{id}")
    @Transactional
    public void deleteById(@PathVariable("id") String id) throws NotFoundException {
        if (id == null)
            throw new NotFoundException(id);
        domainRepository.deleteById(id);
    }

    @GetMapping("/find")
    @Transactional
    public List<DomainDTO> find(@RequestParam("domain") String domain) {
        if (domain == null)
            return all();
        return domainRepository.findByDomain(domain)
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @PostMapping
    public DomainDTO createDomain(@RequestBody CreateDomainDTO createDomainDTO) {
        Domain domain = new Domain(createDomainDTO.getDomain());
        domainRepository.save(domain);
        return toDTO(domain);
    }

    @PostMapping("/{id}")
    public DomainDTO updateDomain(@PathVariable("id") String id, @RequestBody UpdateDomainDTO updateDomainDTO) throws NotFoundException {
        Domain domain = domainRepository.findById(id).orElseThrow(() -> new NotFoundException(id));
        domain.setDomain(updateDomainDTO.getDomain());
        domainRepository.save(domain);
        return toDTO(domain);
    }

    private DomainDTO toDTO(Domain domain) {
        return new DomainDTO(
                domain.getId(),
                domain.getDomain(),
                domain.getRights().stream()
                        .map(Rights::getKey)
                        .collect(Collectors.toList())
        );
    }

}
