package com.example.iam.controller;

import com.example.iam.domain.Domain;
import com.example.iam.domain.Rights;
import com.example.iam.dto.CreateRightsDTO;
import com.example.iam.dto.RightsDTO;
import com.example.iam.exceptions.NotFoundException;
import com.example.iam.repo.DomainRepository;
import com.example.iam.repo.RightsRepository;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/rights")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RightsController {

    RightsRepository rightsRepository;
    DomainRepository domainRepository;

    public RightsController(RightsRepository rightsRepository,
                            DomainRepository domainRepository) {
        this.rightsRepository = rightsRepository;
        this.domainRepository = domainRepository;
    }

    @GetMapping
    public List<RightsDTO> all() {
        return StreamSupport.stream(rightsRepository.findAll().spliterator(), false)
                .map(this::toDTO)
                .collect(Collectors.toList());
    }


    @PostMapping
    @Transactional
    public RightsDTO createRights(@RequestBody CreateRightsDTO createRightsDTO) throws NotFoundException {
        String domainName = createRightsDTO.getDomain();
        Optional<Domain> domainOptional = domainRepository.findByDomain(domainName)
                .findAny();
        if (domainOptional.isEmpty())
            throw new NotFoundException("Domain with name "+domainName+" not found!");

        Rights rights = new Rights(
                domainOptional.get(),
                createRightsDTO.getKey());
        rightsRepository.save(rights);
        return toDTO(rights);
    }

    private RightsDTO toDTO(Rights rights) {
        return new RightsDTO(
                rights.getId(),
                rights.getKey(),
                rights.getDomain().getDomain()
        );
    }

}
