package com.example.iam.controller;

import com.example.iam.domain.Domain;
import com.example.iam.domain.Rights;
import com.example.iam.domain.User;
import com.example.iam.dto.*;
import com.example.iam.exceptions.NotFoundException;
import com.example.iam.repo.DomainRepository;
import com.example.iam.repo.UserRepository;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    UserRepository userRepository;
    DomainRepository domainRepository;

    public UserController(UserRepository userRepository, DomainRepository domainRepository) {
        this.userRepository = userRepository;
        this.domainRepository = domainRepository;
    }

    @GetMapping
    public List<UserDTO> all() {
        return StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/find")
    @Transactional
    public List<UserDTO> find(@RequestParam("username") String username) {
        if (username == null)
            return all();
        return userRepository.findByUsername(username)
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @PostMapping
    public UserDTO createUser(@RequestBody CreateUserDTO createUserDTO) {
        User user = new User(
                createUserDTO.getUsername(),
                "" + createUserDTO.getPassword().hashCode(),
                UUID.randomUUID().toString());
        userRepository.save(user);
        return toDTO(user);
    }

    @PostMapping("/{id}")
    public UserDTO updateUser(@PathVariable("id") String id, @RequestBody UpdateUserDTO updateUserDTO) throws NotFoundException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(id));
        user.setUsername(updateUserDTO.getUsername());
        user.setEncryptedPassword("" + updateUserDTO.getPassword().hashCode());
        userRepository.save(user);
        return toDTO(user);
    }

    @PostMapping("/addRightToUser")
    @Transactional
    public UserDTO addRightToUser(@RequestBody AddRightToUserDTO addRightToUserDTO) throws NotFoundException {
        return applyToUser(
                addRightToUserDTO.getUsername(),
                addRightToUserDTO.getDomain(),
                addRightToUserDTO.getRight(),
                User::addRight)
                .map(this::save)
                .map(this::toDTO)
                .get();
    }

    @DeleteMapping("/removeRightFromUser")
    @Transactional
    public UserDTO removeRightFromUser(@RequestBody RemoveRightFromUserDTO removeRightFromUserDTO) throws NotFoundException {
        return applyToUser(
                removeRightFromUserDTO.getUsername(),
                removeRightFromUserDTO.getDomain(),
                removeRightFromUserDTO.getRight(),
                User::delRight)
                .map(this::save)
                .map(this::toDTO)
                .get();
    }

    @DeleteMapping("/{id}")
    @Transactional
    public void deleteById(@PathVariable("id") String id) throws NotFoundException {
        if (id == null)
            throw new NotFoundException(id);
        userRepository.deleteById(id);
    }


    private UserDTO toDTO(User user) {
        return new UserDTO(
                user.getId(),
                user.getUsername(),
                user.getRights().stream()
                        .map(rights -> rights.getDomain().getDomain() + "." + rights.getKey())
                        .collect(Collectors.toList())
        );
    }

    @FunctionalInterface
    interface UserRight {
        void apply(User user, Rights rights);
    }

    private User save(User user) {
        return userRepository.save(user);
    }

    private Optional<User> applyToUser(String username, String domainName, String rightName, UserRight userRight) throws NotFoundException {
        Optional<User> userOptional = userRepository.findByUsername(username)
                .findAny();
        if (userOptional.isEmpty())
            throw new NotFoundException("User with name '" + username + "' not found!");

        Optional<Domain> domainOptional = domainRepository.findByDomain(domainName)
                .findAny();
        if (domainOptional.isEmpty())
            throw new NotFoundException("Domain with name '" + domainName + "' not found!");

        Domain domain = domainOptional.get();

        Optional<Rights> rightsOptional = domain.getRights().stream()
                .filter(r -> r.getKey().equals(rightName))
                .findAny();
        if (rightsOptional.isEmpty())
            throw new NotFoundException("Right with name '" + rightName + "' not found!");

        User user = userOptional.get();
        Rights rights = rightsOptional.get();

        userRight.apply(user, rights);

        return Optional.of(user);
    }
}
