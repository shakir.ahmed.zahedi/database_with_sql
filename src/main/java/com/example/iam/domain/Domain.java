package com.example.iam.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity(name = "domains")
public class Domain {
    @Id String id;
    @Column(name = "domain") String domain;

    @OneToMany(mappedBy = "domain")
    List<Rights> rights;

    protected Domain() {}

    public Domain(String domain) {
        this.id = UUID.randomUUID().toString();
        this.domain = domain;
        this.rights = new ArrayList<>();
    }
}
