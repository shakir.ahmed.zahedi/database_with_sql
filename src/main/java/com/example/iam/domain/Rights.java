package com.example.iam.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity(name = "rights")
public class Rights {
    @Id String id;
    @Column(name = "key_name") String key;

    @ManyToOne()
    @JoinColumn(name = "domain_id")
    Domain domain;

    @ManyToMany(mappedBy = "rights")
    List<User> users;

    protected Rights() {}

    public Rights(Domain domain, String key) {
        this.id = UUID.randomUUID().toString();
        this.domain = domain;
        this.key = key;
        this.users = new LinkedList<>();
    }

}
