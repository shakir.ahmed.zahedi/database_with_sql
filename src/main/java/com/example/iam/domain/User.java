package com.example.iam.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity(name = "users")
public class User {
    @Id String id;
    @Column(name = "username") String username;
    @Column(name = "encrypted_password") String encryptedPassword;
    @Column(name = "password_salt") String passwordSalt;
    @Enumerated(EnumType.STRING) @Column(name = "status") UserStatus userStatus;

    @ManyToMany()
    @JoinTable(name = "user_rights",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "rights_id", referencedColumnName = "id"))
    List<Rights> rights;

    protected User() {}

    public User(String username, String encryptedPassword, String passwordSalt) {
        this.id = UUID.randomUUID().toString();
        this.username = username;
        this.encryptedPassword = encryptedPassword;
        this.passwordSalt = passwordSalt;
        this.userStatus = UserStatus.EMAIL_VALIDATION;
        this.rights = new LinkedList<>();
    }

    public void addRight(Rights rights) {
        this.rights.add(rights);
    }

    public void delRight(Rights rights) {
        this.rights.remove(rights);
    }
}
