package com.example.iam.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class CreateDomainDTO {
    String domain;

    @JsonCreator
    public CreateDomainDTO(@JsonProperty("domain") String domain) {
        this.domain = domain;
    }
}
