package com.example.iam.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class CreateRightsDTO {
    String key;
    String domain;

    @JsonCreator
    public CreateRightsDTO(
            @JsonProperty("key") String key,
            @JsonProperty("domain") String domain) {
        this.key = key;
        this.domain = domain;
    }
}
