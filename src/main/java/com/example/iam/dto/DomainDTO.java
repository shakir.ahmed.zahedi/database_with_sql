package com.example.iam.dto;

import lombok.Value;

import java.util.List;

@Value
public class DomainDTO {
    String id;
    String domain;
    List<String> rights;
}
