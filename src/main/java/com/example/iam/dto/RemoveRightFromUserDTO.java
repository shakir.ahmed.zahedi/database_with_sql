package com.example.iam.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class RemoveRightFromUserDTO {
    String username;
    String right;
    String domain;

    @JsonCreator
    public RemoveRightFromUserDTO(@JsonProperty("username") String username,
                                  @JsonProperty("right") String right,
                                  @JsonProperty("domain") String domain) {
        this.username = username;
        this.right = right;
        this.domain = domain;
    }
}
