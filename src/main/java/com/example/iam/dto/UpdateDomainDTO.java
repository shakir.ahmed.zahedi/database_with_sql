package com.example.iam.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class UpdateDomainDTO {
    String domain;

    @JsonCreator
    public UpdateDomainDTO(@JsonProperty("domain") String domain) {
        this.domain = domain;
    }
}
