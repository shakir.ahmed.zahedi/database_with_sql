package com.example.iam.dto;

import lombok.Value;

@Value
public class UpdateUserDTO {
    String username;
    String password;

}
