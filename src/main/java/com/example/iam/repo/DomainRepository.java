package com.example.iam.repo;

import com.example.iam.domain.Domain;
import org.springframework.data.repository.CrudRepository;

import java.util.stream.Stream;

public interface DomainRepository extends CrudRepository<Domain,String> {

    Stream<Domain> findByDomain(String domain);

}
