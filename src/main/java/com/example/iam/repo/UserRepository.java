package com.example.iam.repo;

import com.example.iam.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.stream.Stream;

public interface UserRepository extends CrudRepository<User,String> {

    Stream<User> findByUsername(String username);
}
