CREATE TABLE users (
    id varchar(100),
    encrypted_password varchar(200),
    username varchar(100),
    password_salt varchar(200),
    primary key(id)
);

