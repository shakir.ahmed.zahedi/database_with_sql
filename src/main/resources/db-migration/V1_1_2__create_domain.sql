CREATE TABLE IF NOT EXISTS domains
(
    id     varchar(100),
    domain varchar(200),
    primary key (id)
);
