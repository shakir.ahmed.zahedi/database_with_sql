CREATE TABLE rights
(
    id        varchar(100),
    key_name  varchar(200),
    domain_id varchar(100),
    primary key (id),
    foreign key (domain_id) references domains (id)
);

CREATE TABLE user_rights
(
    user_id   varchar(100),
    rights_id varchar(100),
    primary key (user_id,rights_id),
    foreign key (user_id) references users (id),
    foreign key (rights_id) references rights (id)
);
