alter table users
    modify column username varchar(100) not null unique;

alter table domains
    modify column domain varchar(100) not null unique;

create unique index unique_rights on rights (
    key_name, domain_id
);